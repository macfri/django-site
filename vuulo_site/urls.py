import settings

from django.conf.urls import patterns, include, url


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'vuulo_site.views.home', name='home'),
    # url(r'^vuulo_site/', include('vuulo_site.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:

    url(r'^user/add/$', 'app.views.user_add'),
    url(r'^user/list/$', 'app.views.user_list'),
    url(r'^user/$', 'app.views.user'),
    #url(r'^user/(.d+)$', 'app.views.user_view'),
    url(r'^user/(?P<id>\d+)$', 'app.views.user_view'),
    

    url(r'^admin/', include(admin.site.urls)),

    url(r'^tinymce/', include('tinymce.urls'))
)



if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))
