import sys
from PIL import Image


def thumbnail(data, width, height,
    force=True, crop=True, adjust_to_width=True):
    """
    :force Si la imagen debe reducirse al tamano exacto proporcionado.

    :crop Si force es verdadero entonces indica que el thumb se creara a
    partir de centrar y cortar.

    :adjust_to_width Solo si force es False, si la imagen debe ajustarse al
    width proporcionado. Sirve en el caso que height sea mayor que el ancho
    y se necesita ajustar respecto a este ultimo. Usa el metodo thumbnail de
    PIL.

    src = '/tmp/foo.jpg'
    src = Image.open(src)
    for i, s in enumerate((
        (100, 285),
        (200, 100),)):
        x = thumbnail(src, *s, force=False)
        x.save('/tmp/%s.jpg' % i)
    """
    if isinstance(data, (str, unicode)):
        img = Image.open(data)
    else:
        img = data if force else data.copy()

    src_width, src_height = img.size

    if not force:
        if adjust_to_width and src_height > src_width:
            height = sys.maxint
        img.thumbnail((width, height), Image.ANTIALIAS)
    else:
        src_ratio = float(src_width) / float(src_height)
        dst_width, dst_height = width, height
        dst_ratio = float(dst_width) / float(dst_height)

        if dst_ratio < src_ratio:
            crop_height = src_height
            crop_width = crop_height * dst_ratio
            x_offset = float(src_width - crop_width) / 2
            y_offset = 0
        else:
            crop_width = src_width
            crop_height = crop_width / dst_ratio
            x_offset = 0
            y_offset = float(src_height - crop_height) / 3

        if crop:
            img = img.crop((
                int(x_offset), int(y_offset),
                int(x_offset) + int(crop_width),
                int(y_offset) + int(crop_height)
            ))
        else:
            dst_height = int(dst_width / src_ratio)

        img = img.resize((dst_width, dst_height), Image.ANTIALIAS)

    return img

def thumbnail(data, width, height,
    force=True, crop=True, adjust_to_width=True):
    """
    :force Si la imagen debe reducirse al tamano exacto proporcionado.

    :crop Si force es verdadero entonces indica que el thumb se creara a
    partir de centrar y cortar.

    :adjust_to_width Solo si force es False, si la imagen debe ajustarse al
    width proporcionado. Sirve en el caso que height sea mayor que el ancho
    y se necesita ajustar respecto a este ultimo. Usa el metodo thumbnail de
    PIL.

    src = '/tmp/foo.jpg'
    src = Image.open(src)
    for i, s in enumerate((
        (100, 285),
        (200, 100),)):
        x = thumbnail(src, *s, force=False)
        x.save('/tmp/%s.jpg' % i)
    """
    if isinstance(data, (str, unicode)):
        img = Image.open(data)
    else:
        img = data if force else data.copy()

    src_width, src_height = img.size

    if not force:
        if adjust_to_width and src_height > src_width:
            height = sys.maxint
        img.thumbnail((width, height), Image.ANTIALIAS)
    else:
        src_ratio = float(src_width) / float(src_height)
        dst_width, dst_height = width, height
        dst_ratio = float(dst_width) / float(dst_height)

        if dst_ratio < src_ratio:
            crop_height = src_height
            crop_width = crop_height * dst_ratio
            x_offset = float(src_width - crop_width) / 2
            y_offset = 0
        else:
            crop_width = src_width
            crop_height = crop_width / dst_ratio
            x_offset = 0
            y_offset = float(src_height - crop_height) / 3

        if crop:
            img = img.crop((
                int(x_offset), int(y_offset),
                int(x_offset) + int(crop_width),
                int(y_offset) + int(crop_height)
            ))
        else:
            dst_height = int(dst_width / src_ratio)

        img = img.resize((dst_width, dst_height), Image.ANTIALIAS)

    return img
