from django.shortcuts import render_to_response, get_object_or_404

from app.models import WebUser

from django.core.paginator import Paginator


def user(request):
    items = WebUser.objects.all()
    return render_to_response(
        'site/user/index.html',
        dict(items=items)
    )


def user_add(request):

    from admin import WebUserForm

    form = WebUserForm

    #return render_to_response(
        #'site/user/add.html',
        #dict(form=form)
    #)

    return render_to_response(
        'site/product/add.html'
    )


def user_view(request, id):

    data = get_object_or_404(WebUser, id=id)
    return render_to_response(
        'site/user/view.html',
        dict(data=data)
    )


def user_list(request):

    users = WebUser.objects.all()

    paginator = Paginator(users, 1)

    try:
        page = int(request.GET.get('page', '1'))
    except Exception as exc:
        print exc
        page = 1

    try:
        users = paginator.page(page)
    except:
        users = paginator.page(paginator.num_pages)

    print users.object_list
    print users.paginator.num_pages

    return render_to_response('site/user/list.html',
        dict(users=users, user=request.user))
