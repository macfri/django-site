from django import forms
from django.contrib import admin

from app.models import WebUser, Category, Product

#from django.contrib.admin import BooleanFieldListFilter
#from django.forms.extras.widgets import SelectDateWidget

#from vuulo_site.utils import thumbs
#from site.utils import thumbs
#from utils import thumbs


class WebUserForm(forms.ModelForm):

    class Meta:
        model = WebUser
        widgets = {
            'password': forms.PasswordInput(),
            'gender': forms.RadioSelect(choices=[(True, 'Yes'), (False, 'No')])
        }


class WebUserAdmin(admin.ModelAdmin):

    form = WebUserForm
    list_display = ('name',  'status', 'lastname')


class CategoryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)
        self.fields['title'].label = 'What song are you going to sing?'

    class Meta:
        model = Category


    def clear_title(self):
        print "sdsdsd"


class CategoryAdmin(admin.ModelAdmin):

    form = CategoryForm
    list_display = ('title', 'status', 'created_at',
        'updated_at', 'total_products')
    list_filter = ('title', 'status')
    ordering = ('-created_at', '-updated_at')

    def save_model(self, request, obj, form, change):
        super(CategoryAdmin, self).save_model(request, obj, form, change)

    def clear_title(self):
        print "sdsdsd"


class ProductForm(forms.ModelForm):

    class Meta:
        model = Product


class ProductAdmin(admin.ModelAdmin):

    form = ProductForm

    list_filter = ('title', 'status', 'type', 'category', 'user')
    ordering = ('-created_at', '-updated_at')
    list_display = ('title', 'status', 'type', 'user', 'category',
        'created_at', 'updated_at')

    def save_model(self, request, obj, *args):

        """
        import StringIO
        from django.core.files import File

        raw_data = request.FILES.get('image').read()
        #raw_data = request.FILES.get('image')

        f = StringIO.StringIO()
        f.write(raw_data)
        raw_data = File(f)

        obj.image.save('jojo', raw_data)
        obj.save()
        """

        """
        from magic import Magic
        import os

        path = os.path.join(upload_path, str(self.slug))
        fext = self.EXTENSIONS.get(
                Magic(mime=True).from_buffer(raw_data))

        if not fext:
            raise Exception('Invalid image')

        try:
            os.makedirs(path)
        except OSError as exc:
            logging.error(exc)

        image = Image_.open(StringIO(raw_data))
        image.save(os.path.join(path, 'original%s' % fext))
        self.size = image.size
        self.fext = fext
        super(Image, self).save()

        for width, height, suffix in self.THUMB_SIZES:
            thumb = thumbnail(image, width, height, force=True,
                    crop=True, adjust_to_width=True)
            thumb.save(
                os.path.join(path, '%s%s' % (suffix, fext))
            )
        """

        obj.save()


admin.site.register(WebUser, WebUserAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
