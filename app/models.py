# -*- coding: utf-8 -*-
import os
import hashlib
import shutil

from datetime import datetime, timedelta

from django.db import models
from django.db.models import ForeignKey
from django.template.defaultfilters import slugify

from utils.fs import OverwriteStorage
from utils.thumbs import ImageWithThumbsField


class WebUser(models.Model):

    GENDER = (('M', 'Masculino'),
            ('F', 'Femenino'))

    STATES = (('active', 'active'),
            ('inactive', 'inactive'))

    email = models.EmailField(
        verbose_name=(u'Correo electrónico'),
        null=False
    )

    name = models.CharField(
        max_length=100,
        null=False,
        verbose_name=(u'Nombre')
    )

    lastname = models.CharField(
        max_length=100,
        null=False,
        verbose_name=(u'Apellido')
    )

    gender = models.CharField(
        max_length=1,
        null=False,
        choices=GENDER,
        verbose_name=(u'Sexo')
    )

    password = models.CharField(
        max_length=255,
        verbose_name=(u'Clave'),
        null=True
    )

    status = models.CharField(
        max_length=20,
        default=STATES[0][1],
        choices=STATES,
        verbose_name=(u'Estado')
    )

    score = models.IntegerField(
        default=0,
        editable=False,
        verbose_name=(u'Puntos')
    )

    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=(u'Fecha de creación')
    )

    modified_at = models.DateTimeField(
        auto_now=True,
        verbose_name=(u'Fecha de edición')
    )

    last_login_at = models.DateTimeField(
        editable=False,
        null=True,
        verbose_name=(u'Ultima fecha de ingreso')
    )

    activation = models.CharField(
        max_length=255,
        null=True,
        editable=False,
    )

    activation_passwd = models.CharField(
        max_length=255,
        null=True,
        editable=False
    )

    facebook_id = models.CharField(
        max_length=255,
        null=True,
        editable=False
    )

    class Meta:
        ordering = ['-created_at']
        verbose_name = 'Web Usuario'

    @classmethod
    def email_exists(self, email):
        return True if self.objects.filter(
        email=self.email).count() > 0 else False

    @classmethod
    def auth(self, email, password):
        password = str(hashlib.sha224(
            password).hexdigest())
        return True if self.objects.filter(
            email=email,
            password=password,
            status='active').count() > 0 else False

    def set_password(self, value):
        if isinstance(value, unicode):
            value = value.encode('utf8')
        self.password = hashlib.sha1(value).hexdigest()

    def save(self, *args, **kwargs):
        if self.password:
            self.set_password(self.password)
        super(WebUser, self).save(*args, **kwargs)

    def __unicode__(self):
        return '%s %s' % (self.name, self.lastname)


class Category(models.Model):

    STATES = (('active', 'active'),
            ('inactive', 'inactive'))

    title = models.CharField(
        max_length=200,
        null=False,
        verbose_name=(u'Titulo')
    )

    slug = models.CharField(
        max_length=255,
        null=False,
        editable=False,
        unique=True
    )

    status = models.CharField(
        null=False,
        default=STATES[0][1],
        max_length=255,
        choices=STATES,
        verbose_name=(u'Estado')
    )

    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=(u'Fecha de creación')
    )

    updated_at = models.DateTimeField(
        auto_now=True,
        verbose_name=(u'Fecha de edición')
    )

    total_products = models.IntegerField(
        default=0,
        editable=False,
        verbose_name=(u'total de productos')
    )

    total_products_active = models.IntegerField(
        default=0,
        editable=False,
        verbose_name=(u'Fecha de creación activos')
    )

    total_products_inactive = models.IntegerField(
        default=0,
        editable=False,
        verbose_name=(u'total de productos inactivos')
    )

    total_products_have = models.IntegerField(
        default=0,
        editable=False,
        verbose_name=(u'total de productos que tiene')
    )

    total_products_want = models.IntegerField(
        default=0,
        editable=False,
        verbose_name=(u'total de productos que quiere')
    )

    class Meta:
        ordering = ['-created_at']
        verbose_name = (u'Categoria')

    @classmethod
    def _generate_slug(cls, title):
        _count = 1
        _slug = slugify(title)
        while len(cls.objects.filter(slug=_slug)) > 0:
            _slug = slugify(u'%s %s' % (title, _count))
            _count += 1
        return _slug

    def save(self, *args, **kwargs):
        if self.slug == '':
            self.slug = self._generate_slug(self.title)
        super(Category, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.title


class Product(models.Model):

    def upload_image(instance, filename):
        if '.' in filename:
            new_filename = '%s.%s' % ('original', filename.split('.')[1])
        else:
            new_filename = '%s' % (instance.slug)
        return '%s/%s/%s' % ('products', instance.slug, new_filename)

    THUMB_SIZES_IMAGES = (
        (640, 428),
        (377, 286),
        (165, 132),
        (88, 69),
    )

    AUDIENCES_TYPES = (('all', 'all'),
                        ('local', 'local'),
                        ('private group', 'private group'))

    TYPES = (('H', 'have'),
            ('W', 'want'))

    STATES = (('active', 'active'),
            ('inactive', 'inactive'))

    title = models.CharField(
        max_length=200,
        null=False,
        db_index=True,
        verbose_name=(u'Titulo')
    )

    slug = models.CharField(
        max_length=255,
        null=False,
        db_index=True,
        editable=False,
        unique=True
    )

    description = models.TextField(
        verbose_name=(u'Descripción'),
        null=False
    )

    cost = models.IntegerField(
        null=False,
        verbose_name=(u'Precio')
    )

    will_travel = models.IntegerField(
        null=False,
        verbose_name=(u'Medida de viaje')
    )

    allow_retailers = models.BooleanField(
        default=False,
        verbose_name=(u'Pedir minoristas')
    )

    allow_charities = models.BooleanField(
        default=False,
        verbose_name=(u'Pedir caridad')
    )

    donate_now = models.BooleanField(
        default=False,
        verbose_name=(u'Donar ahora')
    )

    status = models.CharField(
        null=False,
        default='inactive',
        db_index=True,
        max_length=20,
        choices=STATES,
        verbose_name=(u'Estado')
    )

    created_at = models.DateTimeField(
        auto_now_add=True,
        db_index=True,
        verbose_name=(u'Fecha de creación')
    )

    updated_at = models.DateTimeField(
        auto_now=True,
        db_index=True,
        verbose_name=(u'Fecha de edición')
    )

    published_at = models.DateTimeField(
        db_index=True,
        default=datetime.now(),
        verbose_name=(u'Fecha de publicación'),
        help_text="""Por favor, usar el siguiente
            formato <strong>dd/mm/yyyy</strong>"""
    )

    expires_at = models.DateTimeField(
        db_index=True,
        default=datetime.now() + timedelta(days=5),
        verbose_name=(u'Fecha de expiración'),
        help_text="""Por favor, usar el siguiente
            formato <strong>dd/mm/yyyy</strong>"""
    )

    type = models.CharField(
        choices=TYPES,
        db_index=True,
        max_length=1,
        verbose_name=(u'Tipo')
    )

    user = ForeignKey(
        WebUser,
        verbose_name=(u'Propietario')
    )

    category = ForeignKey(
        Category,
        verbose_name=(u'Categoria')
    )

    image = ImageWithThumbsField(
        upload_to=upload_image,
        sizes=THUMB_SIZES_IMAGES,
        storage=OverwriteStorage(),
        verbose_name=(u'Imagen')
    )

    location = models.CharField(
        max_length=255,
        verbose_name=(u'Localización')
    )

    audience = models.CharField(
        max_length=255,
        choices=AUDIENCES_TYPES,
        verbose_name=(u'Audiencia')
    )

    class Meta:
        ordering = ['-created_at']
        verbose_name = (u'Producto')

    @classmethod
    def _generate_slug(cls, title):
        _count = 1
        _slug = slugify(title)
        while cls.objects.filter(slug=_slug).count() > 0:
            _slug = slugify(u'%s %s' % (title, _count))
            _count += 1
        return _slug

    def save(self, *args, **kwargs):
        if self.slug == '':
            self.slug = self._generate_slug(self.title)
        super(Product, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        base_path = os.path.dirname(self.image.path)
        shutil.rmtree(base_path)
        super(Product, self).delete(*args, **kwargs)

    def __unicode__(self):
        return self.title
